clear all 
close all
clc

for i=1:100
    Hlam = sprandsym(2000,0.1);
    [minEigHlamHat1, time1(i)] = computeSmallestEig(Hlam, 'full');
    [minEigHlamHat2, time2(i)] = computeSmallestEig(Hlam, 'sparse');
    [minEigHlamHat3, time3(i)] = computeSmallestEig(Hlam, 'mysvds');
    [minEigHlamHat4, time4(i)] = computeSmallestEig(Hlam, 'sparseMax');
    
    checkEqualWithTol(minEigHlamHat1,minEigHlamHat2,1e-3)
    checkEqualWithTol(minEigHlamHat1,minEigHlamHat3,1e-3)
    checkEqualWithTol(minEigHlamHat1,minEigHlamHat4,1e-3)
end

fprintf('full, ave time: %f\n', mean(time1))
fprintf('sparse, ave time: %f\n', mean(time2))
fprintf('svd, ave time: %f\n', mean(time3))
fprintf('sparseMax, ave time: %f\n', mean(time4))
