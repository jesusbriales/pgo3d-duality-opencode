function checkLinearModelPose3D(A, b, M, rotationEstimate, halfCostPose3D)
% test that Q is correct

nrNodes = length(rotationEstimate);
t = zeros(3*nrNodes,1);
r = zeros(9*nrNodes,1);
for i=1:nrNodes
  Ri = rotationEstimate(i).R;
  
  indRow = blockToMatIndices(i, 9);
  r(indRow) = [Ri(1,:)' ; Ri(2,:)'; Ri(3,:)'];
  
  indRow = blockToMatIndices(i, 3);
  t(indRow) = rotationEstimate(i).t;
end
x = [t; r];
if ( norm(0.5 * x' * M * x - halfCostPose3D) > 1e-4 ) error('error mismatch (M)'); end

ancInd_t1 = [1 2 3];
ancInd_r1 = 3*nrNodes + [1:9];
ancInd = [ancInd_t1 ancInd_r1];
indToKeep = setdiff([1:12*nrNodes],ancInd);
xanc = x(indToKeep);
if halfCostPose3D > 1e-3  
  if (norm(0.5*norm(A*xanc - b)^2 - halfCostPose3D) / halfCostPose3D > 1e-4) error('relative error mismatch (A,b)'); end
else
  if (norm(0.5*norm(A*xanc - b)^2 - halfCostPose3D) > 1e-4) error('absolute error mismatch (A,b)'); end
end

if (norm(M - M','inf') > 1e-4) error('nonsymmetric M'); end