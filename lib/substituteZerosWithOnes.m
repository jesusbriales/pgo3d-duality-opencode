function newA = substituteZerosWithOnes(A)

for i=1:size(A,1)
    for j=1:size(A,2)
        if norm(A(i,j)) < 1e-5 % numerical zero
            newA(i,j) = 1;
        else
            newA(i,j) = A(i,j);
        end
    end
end