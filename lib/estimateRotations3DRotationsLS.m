function [estimateRotationOnly, measurements, timeLog]  = estimateRotations3DRotationsLS(measurements, edges_id, outliersRotations, checkTime)
% Author: Luca Carlone, 2014
% This implements the apprach described in R. Hartley, J. Trumpf, Y. Dai,
% H. Li, "Rotation Averaging", IJCV, 2103. 
% The approach is described in Section 7.2
ttot = tic;

%% the original problem to solve is 
% min sum|| Rij Ri(k) - Rj(k) ||^2, s.t. ||r||=1 (solved with SVD as usual)
% ri are now the k-th columns of the rotation matrix ri and rj
m = length(measurements.between);
nrRot = max ( max(edges_id(:,1)), max(edges_id(:,2)) );

t1 = tic;
blockSize = 9;
A9t = spalloc(blockSize*(m+1), blockSize*nrRot, 6*blockSize*(m+1)); % an incidence matrix with 9x9 blocks
b9t = spalloc(blockSize*(m+1), 1, blockSize*(m+1));
I9 = eye(blockSize);
O3 = zeros(3,3);

for k=1:m % for each measurement
  % Ri Rij - Rj = 0 by rows Rij' * ri(1) = rj(1) (ri(1) and rj(1) are the first row of both rotations) 
  Rijtran = measurements.between(k).R';
  i = edges_id(k,1);
  j = edges_id(k,2);
  
  rowIndices = blockToMatIndices(k,blockSize);
  
  colIndicesI = blockToMatIndices(i,blockSize);
  A9t(rowIndices, colIndicesI) = [Rijtran O3 O3; O3  Rijtran  O3;  O3  O3  Rijtran];
  
  colIndicesJ = blockToMatIndices(j,blockSize);
  A9t(rowIndices, colIndicesJ) = -I9;
end
% Prior
rowIndices = blockToMatIndices(m+1,blockSize);
colIndicesI = blockToMatIndices(1,blockSize);
A9t(rowIndices, colIndicesI) = I9;
b9t(rowIndices) = [1 0 0, 0 1 0, 0 0 1]';
timeLog.timeCreateDatafor3DRotations = toc(t1);

t2 = tic;
Rstar = (A9t' * A9t) \ (A9t' * b9t);
timeLog.timeSolveRelaxedRotations = toc(t2);

t3 = tic;
for i=1:nrRot
  indicesI = blockToMatIndices(i,blockSize);
  RiVec = Rstar(indicesI); RiVec(:); % column vector
  RiNonOrt = full([RiVec(1:3)';  RiVec(4:6)'; RiVec(7:9)']);
  % Higham, Matrix nearness problems and applications, Applications of matrix theory, Oxford press, 1989
  estimateRotationOnly(i).R = projectToSO3(RiNonOrt);
end
% warning('Using least squares recovery of rotations')
timeLog.timeNormalizeRotations = toc(t3);

timeLog.total = toc(ttot);

if(checkTime)
  disp(sprintf(' * TIME: \n --> timeCreateDatafor3DRotations: %g, \n --> timeSolveRelaxedRotations: %g, \n --> timeNormalizeRotations: %g', ...
    timeLog.timeCreateDatafor3DRotations,timeLog.timeSolveRelaxedRotations,timeLog.timeNormalizeRotations))
  disp(sprintf(' --> TOTAL: %g',timeLog.timeCreateDatafor3DRotations+timeLog.timeSolveRelaxedRotations+timeLog.timeNormalizeRotations)) 
end

