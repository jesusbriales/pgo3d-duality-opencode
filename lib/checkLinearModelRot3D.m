function checkLinearModelRot3D(A, b, Q, rotationEstimate, rotCost3D)
% test that Q is correct

nrNodes = length(rotationEstimate);
r = zeros(9*nrNodes,1);
for i=1:nrNodes
  Ri = rotationEstimate(i).R;
  indRow = blockToMatIndices(i, 9);
  r(indRow) = [Ri(1,:)' ; Ri(2,:)'; Ri(3,:)'];
end
if ( norm(r' * Q * r - rotCost3D) > 1e-4 ) error('error mismatch (Q)'); end
if (norm(norm(A*r(10:end) - b)^2 - rotCost3D) > 1e-4) error('error mismatch (A,b)'); end
if (norm(Q - Q','inf') > 1e-4) error('nonsymmetric Q'); end