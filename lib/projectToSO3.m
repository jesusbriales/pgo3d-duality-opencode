function R = projectToSO3(RiNonOrt)
% Project matrix to closest rotation
% Higham, Matrix nearness problems and applications, Applications of matrix theory, Oxford press, 1989

if size(RiNonOrt,1) ~= 3 || size(RiNonOrt,2) ~= 3
  error('Input to projectToSO3 is not a 3x3 matrix');
end

[U,S,V] = svd(full(RiNonOrt));
Ri = U * V'; % this is in O(3)
if det(Ri) < 0 
  U(:,3) = -U(:,3);
  Ri = U * V';
  if (abs(det(Ri)-1) > 1e-3); error('Wrong determinant for rotation matrix'); end
end
R = Ri;