function [halfCost3Dang,errPose,errTnorm,errRnormFrob, halfCost3Dfrob] = evaluateCostPose3D(poses, measurements, edges_id, doPlot)

if nargin < 4
  doPlot = 0;
end
if nargin < 3
  edges_id = measurements.edges_id;
end

m = size(edges_id,1);
errPose = zeros(m,1);
errTnorm     = zeros(m,1);
errRnormFrob = zeros(m,1);

for k=1:m % for each measurement
  Rij = measurements.between(k).R;
  tij = measurements.between(k).t;
  
  id1 = edges_id(k,1);
  id2 = edges_id(k,2);
  Ri  = poses(id1).R;
  Rj  = poses(id2).R;
  ti  = poses(id1).t;
  tj  = poses(id2).t;

  ErrTij = (Rij' * (Ri' * (tj - ti) - tij));
  
  [u,th]=logrot(Rij' * Ri' * Rj);
  ErrRij = u * th;

  ErrVect = [ErrTij; ErrRij];
  
  Omegaij = measurements.between(k).Info;
  errPose(k) = 0.5 * ErrVect' * Omegaij * ErrVect;
  
  Omegaijtran = Omegaij(1:3,1:3);
  Omegaijrot = Omegaij(4:6,4:6);
  if norm( Omegaijtran - eye(3)*Omegaijtran(1,1) ,'inf')>1e-5 || norm( Omegaijrot - eye(3)*Omegaijrot(1,1) ,'inf')>1e-5
    warning('Frobenius norm requires isotropic covariances (1)')
  end
  errTnorm(k) = sqrt(Omegaijtran(1,1)) * norm(ErrTij);
  errRnormFrob(k) = sqrt(Omegaijrot(1,1)) * norm(Ri * Rij -  Rj,'fro');
end

if doPlot
  figure;  hold on; plot(errTnorm,'r')
  plot(errRnormFrob,'g'); plot(errPose,'b');
  legend('errTnorm','errRnormFrob','errPose')
end

halfCost3Dang = sum(errPose);
halfCost3Dfrob = 0.5*( sum(errTnorm.^2) + 0.5 * sum(errRnormFrob.^2) );

%% From GTSAM: betweenFactor errpr
% T hx = p1.between(p2, H1, H2); 
% return measured_.localCoordinates(hx);
% 
% p1.between(p2) = p1.inverse * p2 = (R1',-R1'*t1) * (R2,t2) = (R1'*R2, R1'*(t2-t1))
% 
% measured_.localCoordinates(hx) = Logmap(measured_.between(hx)) = 
% (Rij'*R1'*R2, Rij' * (R1'*(t2-t1) -tij))
% 
% Logmap(R,t) = [log(R),t]

