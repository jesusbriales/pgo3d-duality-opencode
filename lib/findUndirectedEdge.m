function [found] = findUndirectedEdge(edge, edges_id)
% The function returns 1 is an edge is found in the edges list (edges_id) or 
% zero otherwise
% edge: row vector of two elements [i j]
% edges_id: m x 2 matrix in which each row describes an edge in a graph
%
% Author: Luca Carlone
% Date: May 2014

id1 = edge(1);
id2 = edge(2);

foundForward = find(edges_id(:,1)==id1 & edges_id(:,2)==id2);
if length(foundForward) == 1
  found = 1;
  return;
end

foundBack = find(edges_id(:,2)==id1 & edges_id(:,1)==id2);
if length(foundBack) == 1
  found = 1;
  return;
end

if length(foundForward) > 1 || length(foundBack) > 1 
  error('Multiple edge!')
end

found = 0;

end