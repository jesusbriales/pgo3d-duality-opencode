function poseEstimateVect = poses3D2Vect(poseEstimate)

if norm(poseEstimate(1).t) > 1e-4 || norm(poseEstimate(1).R - eye(3)) > 1e-4 
  error('poses3D2Vect currently assumes that the first pose in poseEstimate is the anchor (0,I)')
end

nrNodes = length(poseEstimate);
n = nrNodes-1; % number of observable poses

poseEstimateVectTran = zeros(3*n,1);
poseEstimateVectRot = zeros(9*n,1);
for i=2:nrNodes
  indRow = blockToMatIndices(i-1, 9);
  poseEstimateVectRot(indRow) = vec(poseEstimate(i).R'); % rows of the rotation
  
  indRow = blockToMatIndices(i-1, 3);
  poseEstimateVectTran(indRow) = poseEstimate(i).t;
end
poseEstimateVect = [poseEstimateVectTran; poseEstimateVectRot; 1]; % homogeneous form

%% Check
posesTest = vect2Poses3D(poseEstimateVect, nrNodes);
for i=1:nrNodes
  if norm(poseEstimate(i).t - posesTest(i).t) > 1e-4 || norm(poseEstimate(i).R - posesTest(i).R) > 1e-4
    error('something wrong in poses3D2Vect')
  end
end