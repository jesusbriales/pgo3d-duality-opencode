function [] = printFig(Fig, filename)

% set(findobj(gca,'Type','text'),'FontSize',12)
% set(findobj(Fig,'Type','text'),'FontSize',12)
set(Fig, 'paperunits', 'points' )
set(Fig,'position',[100 100, 600 450]); %
set(Fig,'papersize',[600 450]);
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,strrep(filename,'\',''),'pdf'); % epsc