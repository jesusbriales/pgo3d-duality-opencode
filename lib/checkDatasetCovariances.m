function checkDatasetCovariances(measurements)

m = length(measurements.between);

%% Statistics original info matrix
original_std_tran_rot = zeros(m,6);
for k=1:m
  measurements.between(k).originalInfo;
  covariance_k = inv(measurements.between(k).originalInfo);
  original_std_tran_rot(k,:) = diag(covariance_k).^0.5; % standard deviations
end

figure; hold on; title('original std')
subplot(2,1,1); hold on
ylabel('tran std')
plot(original_std_tran_rot(:,1),'-r')
plot(original_std_tran_rot(:,2),'--g')
plot(original_std_tran_rot(:,3),':b')
subplot(2,1,2); hold on
ylabel('rot std')
plot(original_std_tran_rot(:,4),'-r')
plot(original_std_tran_rot(:,5),'--g')
plot(original_std_tran_rot(:,6),':b')


%% Statistics isotropic covariance matrix
std_tran_rot = zeros(m,6);
for k=1:m
  covariance_k = inv(measurements.between(k).Info);
  std_tran_rot(k,:) = diag(covariance_k).^0.5; % standard deviations
end

figure; hold on; title('isotropic std')
subplot(2,1,1); hold on
ylabel('tran std')
plot(std_tran_rot(:,1),'-r')
plot(std_tran_rot(:,2),'--g')
plot(std_tran_rot(:,3),':b')
subplot(2,1,2)
ylabel('rot std'); hold on
plot(std_tran_rot(:,4),'-r')
plot(std_tran_rot(:,5),'--g')
plot(std_tran_rot(:,6),':b')

