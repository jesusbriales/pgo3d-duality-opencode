function poses = odometryFromEdges3D(measurements,nrNodes)
% Builds the odometric path from the delta poses encoded in "edges"
% The function assumes that the first numNodes-1 edges are the *ordered*
% spanning path (i.e., edges are 1->2, 2->3, ..., numNodes-1->numNodes)
%
% Luca Carlone 
% Georgia Institute of Technology 
% July 19, 2014

poses(1).R = eye(3); 
poses(1).t = zeros(3,1); 
for k = 2:nrNodes
  pi = poses(k-1);
  delta_pose = measurements.between(k-1);
  [tj, Rj] = poseAdd3D(pi , delta_pose); 
  poses(k).t = tj;
  poses(k).R = Rj;
end