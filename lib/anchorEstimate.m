function posesInitialization = anchorEstimate(posesInitialization);

if norm(posesInitialization(1).t) > 1e-4 || norm(posesInitialization(1).R - eye(3)) > 1e-4
  warning('posesInitialization has the first node ~= identity pose, we are going to anchor it')
  anchorPose.t = posesInitialization(1).t;
  anchorPose.R = posesInitialization(1).R;
  for i=1:length(posesInitialization)
    currentPose.t = posesInitialization(i).t;
    currentPose.R = posesInitialization(i).R;
    [anchor_t_i,anchor_R_i] = poseSubNoisy3D(anchorPose, posesInitialization(i));
    posesInitialization(i).t = anchor_t_i;
    posesInitialization(i).R = anchor_R_i;
  end
end