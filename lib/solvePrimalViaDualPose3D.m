function [primalViaDualSol,primalViaDualTime] = solvePrimalViaDualPose3D(H_pose_Lam, measurements, costPose3DChord)

nrNodes = measurements.nrNodes;

%% Computing primal solution via dual (v1) - eigenvector
% primal solution is in the null space of H_pose_Lam
[eigevectors, smallestEigs] = eigs(H_pose_Lam, 1, 'SM');
poseEstimateVect_v1 = eigevectors(:,1);
% primal solution has y=1
poseEstimateVect_v1 = poseEstimateVect_v1 / poseEstimateVect_v1(end); % magic
% Convert to pose format
poseEstimateDual_v1 = vect2Poses3D(poseEstimateVect_v1, nrNodes);
% Check cost
[~,~,~,~,costPose3Ddual_v1] = evaluateCostPose3D(poseEstimateDual_v1, measurements);
fprintf('## Optimal cost of primal via dual v1 (Pose3D, anchored, eigvec):\n %.10E <?> cost: %.10E\n', costPose3Ddual_v1, costPose3DChord);
disp('====================================================================')

%% Computing primal solution via dual (v2) - linear system
% In v1 we solved H_pose_Lam [x y] = 0 and then forced y=1,
% in this case we solve H_pose_Lam [x 1] = 0, in which the last column of H_pose_Lam becomes the rhs:
pViaDualTime = tic;
poseEstimateVect_v2 = H_pose_Lam(:,1:end-1) \ (-H_pose_Lam(:,end));
primalViaDualTime = toc(pViaDualTime);
% Convert to pose format
poseEstimateDual_v2 = vect2Poses3D([poseEstimateVect_v2; 1], nrNodes);
% Check cost
[~,~,~,~,costPose3Ddual_v2] = evaluateCostPose3D(poseEstimateDual_v2, measurements);
fprintf('## Optimal cost of primal via dual v2 (Pose3D, anchored, linSys):\n %.10E <?> cost: %.10E\n', costPose3Ddual_v2, costPose3DChord);
disp('====================================================================')

%checkEqualWithTol(costPose3Ddual_v1, costPose3Ddual_v2, 1e-2);

primalViaDualSol.poseEstimateDual_v1 = poseEstimateDual_v1;
primalViaDualSol.costPose3Ddual_v1 = costPose3Ddual_v1;
primalViaDualSol.poseEstimateDual_v2 = poseEstimateDual_v2;
primalViaDualSol.costPose3Ddual_v2 = costPose3Ddual_v2;
primalViaDualSol.H_pose_Lam = H_pose_Lam;