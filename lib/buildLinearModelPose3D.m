function [A, b, Afull, M] = buildLinearModelPose3D(measurements, nrNodes)
% Builds the system matrices for the following least squares problem:
%
% min_(t_i,Ri) sum_ij {omega_t^2 ||tj-ti-Ri*deltaij||^2 + 1/2 omega_r^2 * norm( Ri Rij - Rj , 'fro')  } =
% min_(t_i,ri) sum_ij {omega_t^2 ||tj-ti-Tij * ri||^2 + 1/2 omega_r^2 * || Rijtilde ri - rj ||^2  } 
% = min_{t,r} [t; r]' * M * [t; r] = min ||Afull [t; r]||^2
%
% Luca Carlone
% Georgia Institute of Technology
% Feb 2015

Z33 = zeros(3,3);
I3 = eye(3);
edges_id = measurements.edges_id;
m = length(measurements.between); % num measurements
% Afull = [At3 T ; 0  Q]; % 
At3 = spalloc(3*m, 3*nrNodes, 6*3*m); % 9 nnz per row (conservative)
T = spalloc(3*m, 9*nrNodes, 9*3*m); % 9 nnz per row (conservative)
Q = spalloc(9*m, 9*nrNodes, 9*9*m); % 9 nnz per row (conservative)
ZZ = spalloc(9*m, 3*nrNodes, 0);

for k=1:m 
  Rijt = measurements.between(k).R'; % it's the tranpose!
  Deltaij = measurements.between(k).t;
  
  Omegaij = measurements.between(k).Info;
  Omegaijtran = Omegaij(1:3,1:3);
  Omegaijrot = Omegaij(4:6,4:6);
  if norm( Omegaijtran - eye(3)*Omegaijtran(1,1) ,'inf')>1e-5 || norm( Omegaijrot - eye(3)*Omegaijrot(1,1) ,'inf')>1e-5
      warning('Frobenius norm requires isotropic covariances')
  end
  sqrtInfoTran = sqrt(Omegaijtran(1,1));
  sqrtInfoRot = sqrt(Omegaijrot(1,1)/2); % 2 is because cost if |tran|^2 + 0.5 |rot|^2 
  
  i = edges_id(k,1);
  j = edges_id(k,2);
  
  %% Fill in A3t
  indRow = blockToMatIndices(k, 3); 
  indCol = blockToMatIndices(i, 3);
  At3(indRow, indCol) =  - sqrtInfoTran * I3;
  indCol = blockToMatIndices(j, 3);
  At3(indRow, indCol) = sqrtInfoTran * I3;
  
  %% Fill in T
  indCol = blockToMatIndices(i, 9);
  T(indRow, indCol) =  - sqrtInfoTran * kron(I3,Deltaij');
  
  %% Fill in Q
  indRow = blockToMatIndices(k, 9);
  indCol = blockToMatIndices(i, 9);
  Q(indRow, indCol) =  sqrtInfoRot * kron(I3,Rijt);
  indCol = blockToMatIndices(j, 9);
  Q(indRow, indCol) = - sqrtInfoRot * eye(9);
end

Afull = [At3 T ; ZZ  Q];
M = Afull' * Afull;

% Anchoring: Ax - b
ancInd_t1 = [1 2 3];
ancInd_r1 = 3*nrNodes + [1:9];
ancInd = [ancInd_t1 ancInd_r1];
indToKeep = setdiff([1:12*nrNodes],ancInd);
A = Afull(:,indToKeep); 
b = - Afull(:,ancInd) * [0 0 0, 1 0 0, 0 1 0, 0 0 1]';
