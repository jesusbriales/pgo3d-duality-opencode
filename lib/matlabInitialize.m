function posesInitialization = matlabInitialize(measurements, edges_id, scenario, prinfFig)  

% - Solve for rotations-only
displayTime = 0;
disp(' --> chordal relaxation ')
[estimateRotationOnly, measurements, totTimeLog.timeRotations]  = ...
  estimateRotations3DRotationsLS(measurements, edges_id, [], displayTime);
posesInitialization = estimatePosesGivenRot(measurements, edges_id, estimateRotationOnly);
% plotPose3Graph(posesInitialization, edges_id, 'b', horzcat('rotInit-',scenario), prinfFig)
% costLog.costRotInit = evaluateAngCostRot3D(estimateRotationOnly, measurements, edges_id);
% costLog.costPoseInit = evaluateCostPose3D(posesInitialization, measurements, edges_id);