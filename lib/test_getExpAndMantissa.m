clear all
close all
clc

range = 100;

for test = 1:10000
  scalar = 2*range*(rand-0.5);
  [expo,mant] = getExpAndMantissa(scalar);
  checkEqualWithTol(scalar,mant*10^expo,1e-5);
end

disp('test completed successfully!')