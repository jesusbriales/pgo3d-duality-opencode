function [nrNodes] = writeGridDataset3D(nrX, nrY, nrZ, probLC, sigmaT, sigmaR, filename) 
% Creates a 3D pose graph optimization with ground truth nodes positions 
% arranged as a 3D lattice. The number of "layers" of the lattice are specified 
% by the parameters nrX, nrY, nrZ, such that nrNodes = nrX * nrY * nrZ.
% - probLC: is the probability of loop closures between nearby poses.
% - sigmaT and sigmaR: are the std of the translation and rotation noise
% - filename: name of the file where the pose graph is written (in g2o format)
%
% Author: Luca Carlone
% Georgia Institute of Technology
% Date: May 2014 

% addpath(genpath('../')); % include functionality to write g2o file
doPlot = 1;
I3 = eye(3);

%% default parameters
if nargin < 1
  side = 5;
  nrX = side;
  nrY = side;
  nrZ = side;
  probLC = 0.8;
  sigmaT = 0.1;
  sigmaR = 0.05;
  filename = horzcat('/home/aspn/Desktop/TRO/MOLE2Dmatlab/cleanDatasets/3D/grid3D.g2o');
end

if(doPlot) 
  Fig = figure(); axes('FontSize', 22); hold on
end

%% Build ground truth poses
nrNodes = 0;
for k=1:nrZ
  for i=1:nrX
    for j=1:nrY
      nrNodes = nrNodes+1; % we add a new node
      if i==1 && j ==1 % first node
        poses(nrNodes).R = eye(3);
      else % random rotation 
        % th = 2 * pi * rand - pi; % [-pi, pi]
        % beta = 2 * pi/3 * rand - pi/3; % [-pi/3, pi/3]
        % alpha = pi * rand - pi/2;    % [-pi/2, pi/2]
        % poses(nrNodes).R = rotz(rad2deg(th)) * roty(rad2deg(beta)) * rotx(rad2deg(alpha));
        thVect = 2 * pi * (rand(3,1)-0.5);
        poses(nrNodes).R = rot_exp(I3,rot_hat(I3,thVect));
      end
      if rem(k,2)==1 % odd layers
        if rem(i,2)==1 % odd rows
          poses(nrNodes).t = [j,       i, k]';
        else
          poses(nrNodes).t = [nrY-j+1, i, k]';
        end
      else
        if rem(i,2)==0 % even rows
          poses(nrNodes).t = [j,       nrX-i+1, k]';
        else
          poses(nrNodes).t = [nrY-j+1, nrX-i+1, k]';
        end
      end
      if(doPlot) plot3( poses(nrNodes).t(1), poses(nrNodes).t(2), poses(nrNodes).t(3),'o'); end
    end
  end
end

%% create odometric edges
m = nrNodes-1;
for k=1:m
  id1 = k;
  id2 = k+1;
  edges_id(k,1:2) = [id1 id2];
  [tij,Rij] = poseSubNoisy3D(poses(id1), poses(id2), sigmaT, sigmaR);  
  measurements.between(k).R = Rij;
  measurements.between(k).t = tij;
  measurements.between(k).Info = [(1/sigmaT^2) * eye(3), zeros(3,3);
                                zeros(3,3)       , (1/sigmaR^2) * eye(3) ];
  if(doPlot)
    t1 = poses(id1).t; % gt positions
    t2 = poses(id2).t;
    plot3( [t1(1) t2(1)], [t1(2) t2(2)], [t1(3) t2(3)],'-','lineWidth',2);
  end
end

%% create loop closures
for id1=[1:nrNodes]
  t1 = poses(id1).t; % gt positions
  for id2=[1:nrNodes] %% we look for all possible neighbors (unit distance)
    t2 = poses(id2).t;
    if ( norm(t1 - t2) < 1+0.01 && norm(t1 - t2) > 0.01 ) % unit distance but not the same node
      edge = [id1 id2];
      if (id1==id2) error('coincident nodes?'); end
      if rand<probLC && findUndirectedEdge(edge, edges_id) == 0 % add edge with some probability if it is not there yet
        m = m+1;
        edges_id(m,1:2) = edge;
        [tij,Rij] = poseSubNoisy3D(poses(id1), poses(id2), sigmaT, sigmaR);
        measurements.between(m).R = Rij;
        measurements.between(m).t = tij;
        measurements.between(m).Info = [(1/sigmaT^2) * eye(3), zeros(3,3);
          zeros(3,3)       , (1/sigmaR^2) * eye(3) ];
        if(doPlot) plot3( [t1(1) t2(1)], [t1(2) t2(2)], [t1(3) t2(3)],'--c','lineWidth',2); end
      end
    end
  end
end

if(doPlot) 
  view(3)
  margin = 0.01;
  axis([-margin nrX+margin -margin nrY+margin 1 nrZ+margin])
end
  
poseOdom = odometryFromEdges3D(measurements,nrNodes);
writeG2oDataset3D(filename, measurements, edges_id, poseOdom);