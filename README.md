# README #

### INTRODUCTION ###

* This matlab code creates a 3D pose graph optimization (PGO) problem, and
* 1) iteratively solves the primal problem using Gauss Newton (GN)
* 2) demonstrated the use of the verification techniques presented in [Carlone15iros]

### REQUIREMENTS ###

* the code requires CVX (http://cvxr.com/cvx/) to perform the optimization

### RUNNING THE CODE ###

* execute "main.m" using Matlab

### CITING THIS WORK ###

```
@inproceedings{Carlone15iros,
	Author = {L. Carlone and D. Rosen and G. Calafiore and J.J. Leonard and F. Dellaert},
	Booktitle = iros,
	Title = {Lagrangian Duality in {3D SLAM}: Verification Techniques and Optimal Solutions},
	Year = 2015
}
```

```
@inproceedings{Tron15rssws3D,
	Author = {R. Tron and D. Rosen and L. Carlone},
	Booktitle = {Robotics: Science and Systems (RSS), Workshop ``The problem of mobile sensors: Setting 
	future goals and indicators of progress for SLAM''},
	Title = {On the Inclusion of Determinant Constraints in Lagrangian Duality for 3D SLAM},
	Year = {2015}
}
```

### CONTACTING THE AUTHOR ###
```
% =============================================================================
Luca Carlone
email: lcarlone@mit.edu
website: lucacarlone.com
Massachusetts Institute of Technology
% =============================================================================
```
